package com.a7medkenawy.pagingclass;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApiClient {

    private final static String BASE_URL = "https://api.stackexchange.com/2.2/";
    private static RetrofitApiClient mInstance;
    private Retrofit retrofit;

    private RetrofitApiClient() {

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static RetrofitApiClient getInstance() {
        if (mInstance != null)
            return mInstance;
        else
            return new RetrofitApiClient();
    }

    public API getPref() {
        return retrofit.create(API.class);
    }
}
