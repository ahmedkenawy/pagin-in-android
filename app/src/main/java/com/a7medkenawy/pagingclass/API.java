package com.a7medkenawy.pagingclass;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface API {

    @GET("answers")
    Call<StackApiResponse> getAllData
            (@Query("page") int page, @Query("pagesize") int pagesize, @Query("site") String site);
}
